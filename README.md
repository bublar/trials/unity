# Trial Of Unity

Welcome and thank you for accepting the Unity trial of Bublar!

For clarification, questions or anything else please [send us an email!](mailto:incoming+bublar-trials-unity-20076353-issue-@incoming.gitlab.com)

## Instructions
Create a **private** clone of this repository, and once the assignment is complete share it with GitLab users @adriano.bublar and @aschwel.

Please complete the assignment within 7 days and document the time you spent.

Use [Unity LTS 2019.4.4f1](unityhub://2019.4.4f1/1f1dac67805b) and if you use any additional packages or libraries write a sentence or two explaining why.
## Assignment
Create a [Breakout](https://en.wikipedia.org/wiki/Breakout_(video_game)) style game:
* [Breakout](https://www.youtube.com/watch?v=AMUv8KvVt08)
* [DX Ball](https://www.youtube.com/watch?v=fHX_2DLDp1w)
* [Arkanoid](https://www.youtube.com/watch?v=CS5y9CEUl2g)

It's up to you how far you want to take the concept but the end result must include a paddle, bricks and a ball.
The ball should bounce off the walls, ceiling, paddle and bricks.

We **want to see your creativity**, so feel free to add your own twists and ideas to the game.

We have supplied art assets in the *art* folder in this repository, but you are welcome to create your own or use others.

## For Your Consideration

Write a short note on what you would improve and what parts you think you did best and want us to look at.
